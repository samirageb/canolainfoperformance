var image;
$(document).ready(function(){

    $(".shareholder").click(function(){
		$("#loginFade").fadeIn(300);
		$("#loginBox").delay(200).fadeIn(500);
	});
	
	$("#loginFade, #closeBtn").click(function(){
		$("#loginFade, #loginBox").fadeOut(300);
	});

    //slider 
 /*   $('.vistaImgOFF').camera({
		height: 'auto',
		thumbnails: false,
		hover: false,
		fx: 'scrollHorz',
		navigationHover: false,
		loader: "bar",
		loaderColor: "#00a2ff"
	});
*/
    //this is the SCO plugin code for the fancy box popup
   //  $('.fancybox').magnificPopup(
//     {
//         type: 'image',
//         closeOnContentClick: true,
//         
//         image: {
//             markup: '<div class="mfp-figure">'+
//             '<div class="mfp-close"></div>'+
//             '<div class="mfp-img"></div>'+
//             '<div class="mfp-bottom-bar">'+
//               '<div class="mfp-title"></div>'+
//               '<div class="mfp-counter"></div>'+
//             '</div>'+
//           '</div>',
//         }
//     });
//     
//     
//     $('.fancyboxiframe').magnificPopup(
//     {
//         type:'iframe',
//     
//     });
    
    var error_count = 0;
    //this is the login form for the shareholder access
    $('#login_button').click(function(event) {
    
        $('#login_error').closest('p').stop();
        
        //check the login credentials
        var username = $('#loginUser').val();
        var password = $('#loginPass').val();
        
        $.ajax({
				url: '/members/login.php',
				type: 'post',
				dataType: 'html',
				data: {'login':1, 'username':username, 'password':password},
				success: function(returned) {
					//redirect the location to the members area
					if(returned == 'success')
					{
					    window.location = '/members_only.php';
					}
					else
					{
					    error_count++;
					    
					    if(error_count > 2)
					    {
					        $('#login_error').closest('p').css("opacity", 1);
					        $('#login_error').html(returned + '<p>To reset your password, please call 306.555.5555  or email <a href="mailto:info@domain.com">info@domain.com</a></p>');
					        $('#login_error').closest('p').animate({opacity:0}, 5000);
					    }
					    else
					    {					    
                            $('#login_error').text(returned);
                            $('#login_error').closest('p').css("opacity", 1);
                            $('#login_error').closest('p').animate({opacity:0}, 1000);
                        }
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
				    $('#login_error').text(jqXHR.responseText);
				    $('#login_error').closest('p').css("opacity", 1);
                    $('#login_error').closest('p').animate({opacity:0}, 1000);
				}
			});
    
    });
    
    
    
     $('#logout_button').click(function(event) {
        $.ajax({
				url: '/members/login.php',
				type: 'post',
				dataType: 'html',
				data: {'logout':1},
				success: function(returned) {
					//redirect the location to the members area
					if(returned == 'success')
					{
					    window.location = '/index.php';
					}
					else
					{
					    alert('An error occurred trying to logout!');
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
				    alert('An error occurred with the server trying to logout!');
				}
			});
    
    });
    
    
    
    //twitter stuff
    //<script src="/quadrant/js/Twitter-Post-Fetcher-master/js/twitterFetcher.js"></script>
    var config1 = {
      "id": '585925346312065024',
      "domId": 'tw-widget1',//does nothing since I am doing the callback myself
      "maxTweets": 10,
      "enableLinks": true,
      "showImages": true,
      "showTime": false,
      "showInteraction": false,
      "showUser":false,
      "customCallback": handleTweets
    };

    twitterFetcher.fetch(config1);


    
    function handleTweets(tweets)
    {   
        //console.log('gettonh tweet');
        var x = tweets.length;
        var n = 0;
        var element = $('#twitter_div');//hidden div to hold the formatted tweets
        var html = '';
        element.html(tweets[0]);
    
        //now we get the new domain stuff from twitter and place in the appropriate spots
        var tweet = element.find('.tweet').html();
        media = element.find('.media img').prop('src');
        
        if(tweet)
        {
           /// console.log(tweet);
            $('.tweet').html(tweet);
        }
        
        if(media)
        {
          //  console.log(media);
            $('.tweetmedia').css('background-image', "url('" + media + "')");
        }
        else if(image)
        {
          //  console.log('image: ' + image);
            $('.tweetmedia').css('background-image', "url('" + image + "')");
        }
    }
    
    //FB stuff
    window.fbAsyncInit = function() {
        FB.init({
          appId      : '888409684553146',
          xfbml      : true,
          version    : 'v2.3'
        });
        
        //console.log(FB);
        FB.api(
            "/190791430266/feed", {limit: 1, access_token: '888409684553146|JfdtzkKSOpFSiEdCeER5ETSUo2I'},
            function (response) 
            {
                
                if (response && !response.error) 
                {
                    var message;
                    var picture;
                    var link;
                    
                    /* handle the result */
                    object = response.data;
                    
                    try{
                    message = object[0].message;
                    }
                    catch(e){}
                    
                    try{
                    picture = object[0].picture;
                    }
                    catch(e){}
                    
                    try{
                    link = object[0].link;
                    }
                    catch(e){}
                    
                    if(link)
                    {
                       $('.facebooklink').html(message);
                    }
                    
                    if(message)
                    {
                       /// console.log(tweet);
                        $('.facebookmessage').html(message);
                    }
        
                    if(picture)
                    {
                      //  console.log(media);
                        $('.facebookmedia').css('background-image', "url('" + picture + "')");
                    }
                    else if(image)
                    {
                      //  console.log('image: ' + image);
                        $('.facebookmedia').css('background-image', "url('" + image + "')");
                    }
                    
                   /* for (var property in object) {
                        console.log('property?' + property);
                      console.log(object[property]);
                    }*/
                }
                else
                {
                    alert(response.error.message);
                }
            }
        );
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
       
       
       //pinterest is all PHP so look in custom navigation include for that one
       //here we just take whats on the page and subin anything
       message = $("#pinterest_div #message").html();
       
       if(message)
        {
           //console.log('Pin: ' + message);
            $('.pinterestmessage').html(message);
        }
        
        picture = $("#pinterest_div #picture").html(); 
        
        if(picture)
        {
            //console.log('Picture: ' + picture);
            $('.pinterestmedia').css('background-image', "url('" + picture + "')");
        }

});



