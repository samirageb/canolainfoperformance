jQuery(document).ready(function($) {
  $('.homeslider').royalSlider({
  	autoHeight: false,
    arrowsNav: false,
    loop: true,
    keyboardNavEnabled: true,
    controlsInside: false,
    imageScaleMode: 'fill',
	autoScaleSlider: true, 
    autoScaleSliderWidth: 1480,     
    autoScaleSliderHeight: 438, /*750*/
    slidesSpacing: 0,
    arrowsNavAutoHide: false,
    controlNavigation: 'bullets',
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {
    		// autoplay options go gere
    		enabled: true,
    		pauseOnHover: true,
    		delay: 6000
    	},
    transitionType:'move',
    imgWidth: 1480,
    imgHeight: 438
  });
  
});
/* SAMI EDIT
$(document).ready(function() {
    $(function(){
          var owl = $('.carousel');
          owl.owlCarousel({
            margin:1,
            loop: true,
            nav:true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 2
              },
              1000: {
                items: 3
              }
            }
          })
    });
});


$(function(){
$(window).load(function(){
	$.slidebars({
		disableOver: 767
	});
});
});
*/

$(function(){
	svgeezy.init(false, 'png');
});

$(document).ready(function() {
	$('input, textarea').placeholder();
});

$(document).ready(function() {

var sticky = new Waypoint.Sticky({
  element: $('#topnav')[0]
})
  
});



$(document).ready(function () {
    $('.filteroptions').hide();

    $('.filterselect a').click(function (event) {
        event.preventDefault();
        $('.filteroptions').slideToggle();
        $(this).toggleClass( "open" );
    });
});
/* SAMI EDIT
jQuery(document).ready(function($) {
	$(".modal").fancybox({
		maxWidth	: 600,
		maxHeight	: 600,
		fitToView	: true,
		width		: '70%',
		autoSize	: true,
		margin      : [20, 30, 20, 30],
		'autoScale': true,
		'transitionIn': 'fade',
		'transitionOut': 'fade',
		type: 'ajax',
		helpers		: { 
			overlay : {opacity : 0.7 }
		}
	});
	
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		margin      : [20, 70, 20, 40]
	});
	
	$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		margin      : [20, 70, 20, 40],
		helpers : {
			media : {}
		}
	});


});
*/


$(document).ready(function() {

      $('#subnav .nav a[href*="#"], .subnav a[href*="#"]').click(function(event) {
        event.preventDefault();
        var link = this;
        $.smoothScroll({
          scrollTarget: link.hash
        });
      });
});




$(function(){	

var byRow = $('body').hasClass('test-rows');

	$('.match').each(function() {
        $(this).children('.col').matchHeight({
         byRow: byRow
        //property: 'min-height'
     });
  });
});



$(function(){	
document.querySelector( "#nav-toggle" )
  .addEventListener( "click", function() {
    this.classList.toggle( "active" );
  });
  
	$('.sb-site-container').on('click', function() {
		$("#nav-toggle").removeClass( "active" );
      });
});



$(function(){

// SOCIAL ROLLOVERS
//--------------------------------

// Bind Element
var socialFacebook = $('.social-facebook');
var socialTwitter = $('.social-twitter');

// Toogle position on Rollover
$('.social-facebook').hover(
    function(){
        TweenLite.to(socialFacebook, .4, {css:{left:"-50px"}, delay:0, ease:Expo.easeInOut, overwrite:"all"});
    },  
    function(){
        TweenLite.to(socialFacebook, .4, {css:{left:"-840px"}, delay:0, ease:Expo.easeInOut, overwrite:"all"});
    }
);
$('.social-twitter').hover(
    function(){
        TweenLite.to(socialTwitter, .4, {css:{right:"-50px"}, delay:0, ease:Expo.easeInOut, overwrite:"all"});
    },
    function(){
        TweenLite.to(socialTwitter, .4, {css:{right:"-840px"}, delay:0, ease:Expo.easeInOut, overwrite:"all"});
    }
);

});


$(function(){
	var menu = new cbpTooltipMenu( document.getElementById( 'lang-menu' ) );
});