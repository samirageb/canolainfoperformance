$( document ).ready(function() {

$("#qnc_close_editing_menu").click(function(e){
        e.preventDefault();
		$("#qnc_admin_navigation").hide(300);
	});
	
	$("#qnc_versions_menu").click(function(e){
        e.preventDefault();

        if($('#versions').is(':visible'))
        {
		    hideVersions(300);   
		}
		else
		{
		    showVersions(300);
		}
	});

	$('#select_version').change(function (e){
	    console.log($( this ).val());
	    this.form.submit();
	});
	
	if (typeof $SHOW_VERSIONS !== 'undefined')
	{
        if($SHOW_VERSIONS == 'show')
            showVersions();
    }

    if (typeof $SHOW_ERROR !== 'undefined')
	{    	    
        if($SHOW_ERROR == 'show')
            $("#error_msg").show();
    }
    
    if (typeof $SHOW_MSG !== 'undefined')
	{        
        if($SHOW_MSG == 'show')
        {
            $("#version_msg").show();//.hide(3000);
            $('#version_msg').stop();    
            $('#version_msg').css("opacity", 1);
            $('#version_msg').animate({opacity:0}, 2000, 'swing', function() {$(this).hide()});
        }
    }
});

function showVersions(delay)
{
    $("#versions").show(delay);
    $("#qnc_versions_menu").text('Hide Versions');
}

function hideVersions(delay)
{
    $("#versions").hide(delay);
    $("#qnc_versions_menu").text('Show Versions');
}

